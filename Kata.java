//Complete the solution so that it reverses the string passed into it.

//'world'  =>  'dlrow'
//'word'   =>  'drow'


//!Try solution

public class Kata {

  public static String solution(String str) {
    StringBuilder reversed = new StringBuilder(str);
    return reversed.reverse().toString();
  }

}